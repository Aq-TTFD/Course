/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import daos.StudentDAO;
import entities.Student;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.StudentService;

/**
 *
 * @author admin
 */
@WebServlet(name = "addStudentController", urlPatterns = {"/admin/addStudent"})
public class addStudentController extends HttpServlet {
    
    private StudentService studentservice;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void init() throws ServletException {
        studentservice = StudentService.getInstance();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("admin/addStudent.jsp").forward(request, response);
//        response.sendRedirect("listStudent.jsp");
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String enrollmentDate = request.getParameter("enrollmentDate");
        studentservice.insertNewStudent(email, password, email, dateOfBirth, enrollmentDate);
//        response.sendRedirect("listStudent.jsp");
        
        request.getRequestDispatcher("listStudent.jsp").forward(request, response);
        
    }
    
}
