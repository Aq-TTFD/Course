package controllers;

import entities.Admin;
import entities.Instructor;
import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.AdminService;
import services.InstructorService;
import services.JwtTokenProvider;
import services.StudentService;

import java.io.IOException;

@WebServlet(name="LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {
    private final Integer COOKIE_LIFE_TIME = 60 * 60 * 24;
    private InstructorService instructorService;
    private AdminService adminService;
    private StudentService studentService;
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void init() throws ServletException {
        instructorService = InstructorService.getInstance();
        studentService = StudentService.getInstance();
        adminService = AdminService.getInstance();
        jwtTokenProvider = new JwtTokenProvider();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Instructor instructor = instructorService.getAccountByEmailAndPassword(username, password);
        Admin admin = adminService.getAdminByEmailAndPassword(username, password);
        Student student = studentService.getStudentByEmailAndPassword(username, password);
        if(instructor != null){
            setCookie(resp, "instructor_token", jwtTokenProvider.generateToken(instructor));

        } else if (student != null){
            setCookie(resp, "student_token", jwtTokenProvider.generateToken(student));

            resp.sendRedirect("home");
        } else if (admin != null){
            setCookie(resp, "admin_token", jwtTokenProvider.generateToken(admin));

            resp.sendRedirect("admin-dashboard");
        }
        else {
            req.setAttribute("errorMessage", "Username or Password is invalid");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }

    private void setCookie(HttpServletResponse resp, String cookieName, String token){
        Cookie cookie = new Cookie(cookieName, token);
        cookie.setMaxAge(COOKIE_LIFE_TIME);
        cookie.setDomain("localhost");
        cookie.setPath("/FPT-Nihongo");
        cookie.setHttpOnly(true);
        resp.addCookie(cookie);
    }
}
