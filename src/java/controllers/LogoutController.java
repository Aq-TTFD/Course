package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "LogoutController", urlPatterns = {"/logout"})
public class LogoutController extends HttpServlet {

    // logout?accountType=manager
    // logout?accountType=student
    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accountType = req.getParameter("accountType");

        if (accountType == null || accountType.isEmpty()) {
            logout(req, resp, "all");
        } else {
            logout(req, resp, accountType);
        }
        resp.sendRedirect("login");
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp, String accountType) {
        switch (accountType) {
            case "instructor":
                deleteCookie("instructor_token", req, resp);
                break;
            case "student":
                deleteCookie("student_token", req, resp);
                break;
            case "admin":
                deleteCookie("admin_token", req, resp);
                break;
            default:
                deleteCookie("instructor_token", req, resp);
                deleteCookie("student_token", req, resp);
                deleteCookie("admin_token", req, resp);
                break;
        }
    }

    private void deleteCookie(String name, HttpServletRequest req, HttpServletResponse resp) {
        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    Cookie newCookie = new Cookie(name, "");
                    newCookie.setMaxAge(0);
                    cookie.setDomain("localhost");
                    cookie.setPath("/FPT-Nihongo");
                    resp.addCookie(newCookie);
                    return;
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }
}
