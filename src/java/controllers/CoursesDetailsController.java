/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import entities.CategoryCourse;
import entities.Course;
import entities.Instructor;
import entities.Lecture;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.CategoryCourseService;
import services.CourseService;
import services.InstructorService;
import services.LectureService;

/**
 *
 * @author quang huy
 */
@WebServlet(name = "CoursesDetailsController", urlPatterns = {"/courses-details"})
public class CoursesDetailsController extends HttpServlet {

    private LectureService lectureService;
    private CourseService courseService;
    private InstructorService instructorService;
    private CategoryCourseService categoryCourseService;

    public void init() throws ServletException {
        lectureService = LectureService.getInstance();
        courseService = CourseService.getInstance();
        instructorService = InstructorService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String course_id = request.getParameter("course_id");
        if (course_id == null || course_id.isBlank()) {
            request.getRequestDispatcher("error-404.html").forward(request, response);
            return;
        }
        Course course = courseService.getCourseById(course_id);
        if (course == null) {
            request.getRequestDispatcher("error-404.html").forward(request, response);
            return;
        }
        try {
            List<Lecture> listLecture = lectureService.getAllLectureByCourseId(course_id);
            request.setAttribute("listLecture", listLecture);
            int totalLecture = lectureService.countAllLectureByCourseId(course_id);
            request.setAttribute("totalLecture", totalLecture);
            int totalCourseTimes = lectureService.getTotalCourseHours(course_id);
            request.setAttribute("totalCourseTimes", totalCourseTimes);
            Instructor instructor = instructorService.getInstructorByCourseId(course_id);
            request.setAttribute("instructorName", instructor.getFullName());
            request.setAttribute("instructorSpecialization", instructor.getSpecialization());
            request.setAttribute("instructorEmail", instructor.getEmail());
            CategoryCourse categoryCourse = categoryCourseService.getCategoryCourseByCourseId(course_id);
            request.setAttribute("categoryCourse", categoryCourse.getName());         
            request.setAttribute("nameCourse", course.getName());
            request.setAttribute("description", course.getDescription());
            request.setAttribute("imgURL", course.getImgURL());
            request.setAttribute("priceCourse", course.getPrice());
            request.setAttribute("priceCourseDiscount", course.getPrice() * (1 - course.getDiscount()));
            request.getRequestDispatcher("courses-details.jsp").forward(request, response);
        } catch (ServletException | IOException e) {
            request.getRequestDispatcher("error-404.html").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
