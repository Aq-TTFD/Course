package controllers;

import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.StudentService;
import services.TokenService;

import java.io.IOException;

@WebServlet(name = "ResetPasswordController", value = "/reset-password")
public class ResetPasswordController extends HttpServlet {

    private StudentService studentService;
    private TokenService tokenService;

    @Override
    public void init() throws ServletException {
        tokenService = new TokenService();
        studentService = StudentService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = req.getParameter("token");

        req.setAttribute("token", token);
        req.getRequestDispatcher("reset-password.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String token = req.getParameter("token");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirm-password");

        Student student = (Student) req.getAttribute("student");

        if (!password.equals(confirmPassword)) {
            req.setAttribute("message", "Passwords do not match.");
            req.setAttribute("token", token);
            req.getRequestDispatcher("reset-password.jsp").forward(req, resp);
            return;
        }

        student.setPassword(password);

        if (studentService.updatePasswordStudent(student)) {
            req.setAttribute("message", "Your password has been updated successfully.");
            req.getRequestDispatcher("reset-password.jsp").forward(req, resp);
            return;
        }


        req.setAttribute("message", "Your password has been updated successfully.");
        req.getRequestDispatcher("reset-password.jsp").forward(req, resp);
    }


}
