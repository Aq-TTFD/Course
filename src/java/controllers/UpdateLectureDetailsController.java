/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import entities.Lecture;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.CategoryCourseService;
import services.CourseService;
import services.LectureService;

/**
 *
 * @author quang huy
 */
@WebServlet(name = "UpdateLectureDetailsController", urlPatterns = {"/admin/update-lecture-details"})
public class UpdateLectureDetailsController extends HttpServlet {

    private CategoryCourseService categoryCourseService;
    private CourseService courseService;
    private LectureService lectureService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
        lectureService = LectureService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = (String) request.getParameter("id");

        Lecture lecture = lectureService.getLectureById(id);
        if (lecture == null) {
            response.sendRedirect("delete-lecture");
            return;
        }
        request.setAttribute("id", lecture.getId());
        request.setAttribute("name", lecture.getName());
        request.setAttribute("img", lecture.getImageURL());
        request.setAttribute("description", lecture.getDescription());
        request.setAttribute("video", lecture.getVideoURL());
        request.setAttribute("time", lecture.getTime());

        request.getRequestDispatcher("update-lecture-details.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = (String) request.getParameter("id");

        Lecture lecture = lectureService.getLectureById(id);
        if (lecture == null) {
            response.sendRedirect("delete-lecture");
            return;
        }
        String name = (String) request.getParameter("name");
        String img = (String) request.getParameter("img");
        String time = (String) request.getParameter("time");
        String description = (String) request.getParameter("description");
        String video = (String) request.getParameter("video");

        request.setAttribute("id", lecture.getId());
        request.setAttribute("name", name);
        request.setAttribute("img", img);
        request.setAttribute("description", description);
        request.setAttribute("video", video);
        request.setAttribute("time", time);

        String errorTitle = lectureService.sendErrorAddTitle(name);
        String errorDescription = lectureService.sendErrorAddTitle(description);
        String errorTime = lectureService.sendErrorAddTime(time);
        String errorVideo = lectureService.sendErrorAddTitle(video);
        String errorImg = lectureService.sendErrorAddTitle(img);

        if (errorTitle == null && errorImg == null && errorTime == null && errorVideo == null && errorDescription == null) {
            lectureService.updateLecture(name, time, description, img, video, id);
            request.setAttribute("buildSuccess", "Update lecture successfully!");
        }

        if (errorTime != null) {
            request.setAttribute("errorTime", errorTime);
        }
        if (errorTitle != null) {
            request.setAttribute("errorTitle", errorTitle);
        }
        if (errorDescription != null) {
            request.setAttribute("errorDescription", errorDescription);
        }
        if (errorImg!= null) {
            request.setAttribute("errorImage", errorImg);
        }
        if (errorVideo != null) {
            request.setAttribute("errorVideo", errorVideo);
        }

        request.getRequestDispatcher("update-lecture-details.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
