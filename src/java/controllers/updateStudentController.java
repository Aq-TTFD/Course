/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import daos.StudentDAO;
import entities.Student;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
@WebServlet(name = "updateStudentController", urlPatterns = {"/admin/updateStudent"})
public class updateStudentController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateStudentController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateStudentController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int student_id = Integer.parseInt(request.getParameter("getId()"));
        StudentDAO dao = new StudentDAO();
        try {
            Student s = dao.getStudentById(student_id);
            request.setAttribute("student", s);
            request.getRequestDispatcher("updateStudent.jsp").forward(request, response);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int student_id = Integer.parseInt(request.getParameter("id"));
        String email = request.getParameter("email");
        boolean email_verified = Boolean.parseBoolean(request.getParameter("emailVerified"));
        String phone_number = request.getParameter("phoneNumber");
        boolean active = Boolean.parseBoolean(request.getParameter("active"));
        String password = request.getParameter("password");
        String full_name = request.getParameter("fullName");
        String date_of_birth = request.getParameter("dateOfBirth");
        String picture_Url = request.getParameter("pictureUrl");
        String family_name = request.getParameter("familyName");
        String given_name = request.getParameter("givenName");
        String enrollment_date = request.getParameter("enrollmentDate");
        int role_id = Integer.parseInt(request.getParameter("roleId"));

        StudentDAO dao = new StudentDAO();
        try {

            Student sNew = new Student(student_id, email, email_verified, phone_number, active, password, full_name, date_of_birth, picture_Url, family_name, given_name, enrollment_date, role_id);
            dao.updateStudent(sNew);
            response.sendRedirect("listStudent");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
