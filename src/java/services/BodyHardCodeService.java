/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.BodyHardCodeDAO;
import entities.BodyHardCode;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class BodyHardCodeService {
    private static BodyHardCodeService instance;
    
    private BodyHardCodeDAO bodyHardCodeDAO;

    public BodyHardCodeService() {
        bodyHardCodeDAO = new BodyHardCodeDAO();
    }
    
    public static BodyHardCodeService getInstance(){
        if(instance == null){
            instance = new BodyHardCodeService();
        }
        return instance;
    }
    
    public List<BodyHardCode> getAllBodyHardCode(){
        List<BodyHardCode> result = bodyHardCodeDAO.getAllBodyHardCode();
        if(result == null || result.isEmpty()){
            return null;
        }
        return result;
    }
    public static void main(String[] args) {
        BodyHardCodeService dao = new BodyHardCodeService();
        List<BodyHardCode> list = dao.getAllBodyHardCode();
        for(BodyHardCode i: list){
            System.out.println(i.getId());
        }
    }
}
