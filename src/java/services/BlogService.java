/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.BlogDAO;
import daos.CategoryBlogDAO;
import entities.Blog;
import entities.CategoryBlog;
import java.util.List;

/**
 *
 * @author khanh
 */
public class BlogService {
    private static BlogService instance;

    private BlogDAO blogDAO;

    public BlogService() {
        blogDAO = new BlogDAO();
    }

    public static BlogService getInstance() {
        if (instance == null) {
            instance = new BlogService();
        }
        return instance;
    }

    public List<Blog> getAllBlog() {
        List<Blog> result = blogDAO.getAllBlog();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }
}

//    public List<Blog> getNextBlog(String page, int numberOfBlogInPage) {
//                int pageNumber;
//        if (page == null) {
//            pageNumber = 1;
//        }else{
//            pageNumber = Integer.parseInt(page);
//        }
//        List<Blog> result = blogDAO.get(pageNumber, numberOfBlogInPage);
//        if (result == null || result.isEmpty()) {
//            return null;
//        }
//        return result;
//    }

//    public List<Blog> getNextBlogAfterFilterAll(String categoryIDString, String keyName, int sort, String pageString, int numberOfBlogInPage) {
//        
//        int pageNumber;
//        if (pageString == null) {
//            pageNumber = 1;
//        }else{
//            pageNumber = Integer.parseInt(pageString);
//        }
//        
//        int categoryID;
//        if (categoryIDString == null || categoryIDString.isBlank()) {
//            categoryID = 0;
//        }else{
//            categoryID = Integer.parseInt(categoryIDString);
//        }
//        
//        List<Blog> result = blogDAO.getNextBlogAfterFilterAll(categoryID, keyName, sort, pageNumber, numberOfBlogInPage);
//        if (result == null || result.isEmpty()) {
//            return null;
//        }
//        return result;
//    }

//    public List<Blog> get2RecentBlog() {
//        List<Blog> result = blogDAO.get2RecentBlog();
//        if (result == null || result.isEmpty()) {
//            return null;
//        }
//        return result;
//    }
//
//    public int getNumberOfBlogAfterFilterAll(int categoryID, String keyName) {
//        return courseDAO.getNumberOfBlogAfterFilterAll(categoryID, keyName);
//    }
//
//    public int getNumberOfPageAfterFilterAll(String categoryIDString, String keyName) {
//        
//        int categoryID;
//        if (categoryIDString == null || categoryIDString.isBlank()) {
//            categoryID = 0;
//        }else{
//            categoryID = Integer.parseInt(categoryIDString);
//        }
//        
//        return (int) Math.ceil(courseDAO.getNumberOfBlogAfterFilterAll(categoryID, keyName) * 1.0 / 9);
//    }
//
//    public Blog getBlogById(int id) {
//        return blogDAO.getBlogById(id);
//    }
//    
//    public Blog insertNewBlog(String name, String img, String startDate, 
//                                    String endDate, String description, String categoryIDString, 
//                                    String instructorIDString, String priceString, String discountString){
//        int categoryID;
//        if (categoryIDString == null || categoryIDString.isBlank()) {
//            categoryID = 1;
//        }else{
//            categoryID = Integer.parseInt(categoryIDString);
//        }
//        
//        int instructorID;
//        if (instructorIDString == null || instructorIDString.isBlank()) {
//            instructorID = 1;
//        }else{
//            instructorID = Integer.parseInt(instructorIDString);
//        }
//        
//        double price;
//        if(priceString == null || priceString.isBlank()){
//            price = 0;
//        }else{
//            price = Double.parseDouble(priceString);
//        }
//        
//        double discount;
//        if(discountString == null || discountString.isBlank()){
//            discount = 0;
//        }else{
//            discount = Double.parseDouble(discountString);
//        }
//        Blog course = new Blog(0, name, img, startDate, endDate, description, categoryID, instructorID, 1, price, discount);
//        courseDAO.insertBlog(course);
//        return course;
//    }
//    
//}
