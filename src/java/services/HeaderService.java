/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.HeaderDAO;
import entities.Header;
import java.util.List;


public class HeaderService {

    private static HeaderService instance;
    private HeaderDAO headerDAO;

    private HeaderService() {
        headerDAO = new HeaderDAO();
    }

    public static HeaderService getInstance() {
        if (instance == null) {
            instance = new HeaderService();
        }
        return instance;
    }
    public Header getHeader(String id,String description){
        List<Header> headers = headerDAO.getListHeader();
        for (Header header : headers) {
           if (header.getId().equals(id) && header.getDescription().equals(description)) {
               return header;
        }
        }
        return null;
    }
    
}
