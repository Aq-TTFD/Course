package services;

import daos.AdminDAO;
import entities.Admin;
import org.mindrot.jbcrypt.BCrypt;

public class AdminService {
    private static AdminService instance;

    private AdminDAO adminDAO;
    private AdminService(){
        adminDAO = new AdminDAO();
    }

    public static AdminService getInstance(){
        if(instance == null){
            instance = new AdminService();
        }
        return instance;
    }

    public Admin getAdminByEmailAndPassword(String email, String password){
        Admin admin = adminDAO.findByEmail(email);
        if (admin == null){
            return null;
        }
        if(!BCrypt.checkpw(password, admin.getPassword())){
            return null;
        }
        return admin;
    }
}
