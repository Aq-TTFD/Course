/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.CourseDAO;
import entities.Course;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class CourseService {

    private static CourseService instance;

    private CourseDAO courseDAO;

    public CourseService() {
        courseDAO = new CourseDAO();
    }

    public static CourseService getInstance() {
        if (instance == null) {
            instance = new CourseService();
        }
        return instance;
    }

    public List<Course> getAllCourse() {
        List<Course> result = courseDAO.getAllCourse();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }

    public List<Course> getNextCourse(String page, int numberOfCourseInPage) {
        int pageNumber;
        if (page == null) {
            pageNumber = 1;
        } else {
            pageNumber = Integer.parseInt(page);
        }
        List<Course> result = courseDAO.getNextCourse(pageNumber, numberOfCourseInPage);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }

    public int returnValidID(String iDString) {
        try {
            int id = Integer.parseInt(iDString);

            if (id <= 0 || iDString.isEmpty() || iDString.isBlank()) {
                throw new NumberFormatException();
            }
            return id;
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }

    public int returnValidCategoryID(String categoryIDString) {
        try {
            int id = Integer.parseInt(categoryIDString);

            if (id <= 0 || categoryIDString.isEmpty() || categoryIDString.isBlank()) {
                throw new NumberFormatException();
            }
            return id;
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }

    public int returnValidInstructorID(String instructorIDString) {
        try {
            int id = Integer.parseInt(instructorIDString);

            if (id <= 0 || instructorIDString.isEmpty() || instructorIDString.isBlank()) {
                throw new NumberFormatException();
            }
            return id;
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }

    public int returnValidPageNumber(String pageString) {
        try {
            int page = Integer.parseInt(pageString);

            if (page <= 0 || pageString.isEmpty() || pageString.isBlank()) {
                throw new NumberFormatException();
            }
            return page;
        } catch (NumberFormatException | NullPointerException e) {
            return 1;
        }
    }

    public double returnValidPrice(String priceString) {
        try {
            double price = Double.parseDouble(priceString);

            if (price < 0 || priceString.isEmpty() || priceString.isBlank()) {
                throw new NumberFormatException();
            }
            return price;
        } catch (NumberFormatException | NullPointerException e) {
            return -1;
        }
    }

    public String sendErrorSearchPrice(String startPriceString, String endPriceString) {
        try {
            double startPrice = Double.parseDouble(startPriceString);
            double endPrice = Double.parseDouble(endPriceString);
            if (startPrice < 0 || endPrice < 0) {
                throw new NumberFormatException();
            }
            if (startPrice > endPrice) {
                return "The scope must be valid!";
            }
        } catch (NumberFormatException | NullPointerException e) {
            return "The input must be positive number!";
        }
        return null;
    }

    public List<Course> getNextCourseAfterFilterAll(String categoryIDString, String keyName, String startPriceString, String endPriceString, int sort, String pageString, int numberOfCourseInPage) {

        int categoryID = returnValidCategoryID(categoryIDString);
        int page = returnValidPageNumber(pageString);
        double startPrice;
        double endPrice;

        startPrice = returnValidPrice(startPriceString);
        endPrice = returnValidPrice(endPriceString);

        List<Course> result = courseDAO.getNextCourseAfterFilterAll(categoryID, keyName, startPrice, endPrice, sort, page, numberOfCourseInPage);
        return result;
    }

    public List<Course> get2RecentCourse() {
        List<Course> result = courseDAO.get2RecentCourse();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }

    public int getNumberOfCourseAfterFilterAll(String categoryIDString, String keyName, String startPriceString, String endPriceString) {
        int categoryID = returnValidCategoryID(categoryIDString);
        double startPrice;
        double endPrice;
        if (startPriceString != null && endPriceString != null) {
            startPriceString = startPriceString.replaceAll("\\$", "");
            endPriceString = endPriceString.replaceAll("\\$", "");
        }
        startPrice = returnValidPrice(startPriceString);
        endPrice = returnValidPrice(endPriceString);
        if (startPrice > endPrice) {
            startPrice = -1;
            endPrice = -1;
        }
        return courseDAO.getNumberOfCourseAfterFilterAll(categoryID, keyName, startPrice, endPrice);
    }

    public Integer getNumberOfPageAfterFilterAll(String categoryIDString, String keyName, String startPriceString, String endPriceString, int numberOfCourse) {

        int categoryID = returnValidCategoryID(categoryIDString);
        double startPrice;
        double endPrice;
        if (startPriceString != null && endPriceString != null) {
            startPriceString = startPriceString.replaceAll("\\$", "");
            endPriceString = endPriceString.replaceAll("\\$", "");
        }
        startPrice = returnValidPrice(startPriceString);
        endPrice = returnValidPrice(endPriceString);
        if (startPrice > endPrice) {
            startPrice = -1;
            endPrice = -1;
        }

        return (int) Math.ceil(courseDAO.getNumberOfCourseAfterFilterAll(categoryID, keyName, startPrice, endPrice) * 1.0 / numberOfCourse);
    }

    public Course getCourseById(String idString) {
        int id = returnValidID(idString);
        if (id > courseDAO.getCourseMaxID().getId() && id < courseDAO.getCourseMinID().getId()) {
            return null;
        } else {
            return courseDAO.getCourseById(id);
        }
    }

    public boolean isValidPrice(String priceString) {
        try {
            double price = Double.parseDouble(priceString);

            if (price <= 0) {
                throw new NumberFormatException();
            }
            return true;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public boolean isValidDiscount(String discountString) {
        try {
            double discount = Double.parseDouble(discountString);

            if (discount <= 0 || discount >= 1) {
                throw new NumberFormatException();
            }
            return true;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public String sendErrorAddTitle(String titleString) {
        if (titleString == null || titleString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public String sendErrorAddImg(String imgString) {
        if (imgString == null || imgString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public String sendErrorAddDescription(String descriptionString) {
        if (descriptionString == null || descriptionString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public String sendErrorAddPrice(String priceString) {
        if (priceString == null || priceString.isBlank()) {
            return "This field need to be fill";
        }
        try {
            double price = Double.parseDouble(priceString);

            if (price <= 0) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException | NullPointerException e) {
            return "The price must be positive number!";
        }
        return null;
    }

    public String sendErrorAddDiscount(String discountString) {
        if (discountString == null || discountString.isBlank()) {
            return "This field need to be fill";
        }
        try {
            double discount = Double.parseDouble(discountString);

            if (discount <= 0 || discount >= 1) {
                throw new ArithmeticException();
            }
        } catch (NumberFormatException | NullPointerException e) {
            return "The discount must be the number!";
        } catch (ArithmeticException e) {
            return "The discount must be in scopt from 0 to 1";
        }
        return null;
    }

    public String sendErrorAddDate(String dateString) {
        if (dateString == null || dateString.isBlank()) {
            return "This field need to be fill";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
            sdf.setLenient(false);
            sdf.parse(dateString);
        } catch (ParseException e) {
            return "The input need to be in format day/month/year";
        }
        return null;
    }

    public String sendErrorAddDateScope(String startDateString, String endDateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-mm-dd");
            sdf.setLenient(false);
            Date startDate = sdf.parse(startDateString);
            Date endDate = sdf.parse(endDateString);
            if (endDate.before(startDate)) {
                return "The start date must be occurs before the end date";
            }
        } catch (ParseException e) {
            return "The input need to be in format day/month/year";
        }
        return null;
    }

    public String convertDateIntoDBFormat(String inputDate) {
        String outputDate = "";
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = inputFormat.parse(inputDate);
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDate;
    }

    public String convertDateFromDBFormat(String inputDate) {
        String outputDate = "";
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd/mm/yyyy");
            Date date = inputFormat.parse(inputDate);
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDate;
    }

    public Course insertNewCourse(String name, String img, String startDate,
            String endDate, String description, String categoryIDString,
            String instructorIDString, String priceString, String discountString, String linkMeet) {
        int categoryID = returnValidCategoryID(categoryIDString);
        int instructorID = returnValidInstructorID(instructorIDString);
        double price = Double.parseDouble(priceString);
        double discount = Double.parseDouble(discountString);
        String inputStartDate = startDate; 
        String inputEndDate = endDate; 
        Course course = new Course(0, name, img, inputStartDate, inputEndDate, description, categoryID, instructorID, 1, price, discount, linkMeet);
        courseDAO.insertCourse(course);
        return course;
    }

    public void deactiveCourse(String iDString) {
        int id = returnValidID(iDString);
        courseDAO.deactiveCourseById(id);
    }

    public Course updateCourse(String name, String img, String startDate,
            String endDate, String description, String categoryIDString,
            String instructorIDString, String priceString, String discountString, String linkMeet,  String idString){
        int id = returnValidID(idString);
        int categoryID = returnValidCategoryID(categoryIDString);
        int instructorID = returnValidInstructorID(instructorIDString);
        double price = Double.parseDouble(priceString);
        double discount = Double.parseDouble(discountString);
        String inputStartDate = startDate; 
        String inputEndDate = endDate; 
        Course course = new Course(id, name, img, inputStartDate, inputEndDate, description, categoryID, instructorID, 0, price, discount, linkMeet);
        courseDAO.updateCourse(course);
        return course;
    }   

    public List<Course> getCourseByInstructorId(String idString) {
        int id;
        if (idString == null || idString.isBlank()) {
            id = 0;
        } else {
            try {
                id = Integer.parseInt(idString);
            } catch (NumberFormatException e) {
                id = 0;
            }

        }
        List<Course> course = courseDAO.getCourseByInstructorId(id);

        return course;
    }

}
