/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.CategoryBlogDAO;
import entities.CategoryBlog;
import java.util.List;

/**
 *
 * @author khanh
 */
public class CategoryBlogService {
    private static CategoryBlogService instance;
    
    private CategoryBlogDAO categoryBlogDAO;

    private CategoryBlogService() {
        categoryBlogDAO = new CategoryBlogDAO();
    }
    
    public static CategoryBlogService getInstance(){
        if(instance == null){
            instance = new CategoryBlogService();
        }
        return instance;
    }
    
    public List<CategoryBlog> getAllCategoryBlog(){
        List<CategoryBlog> result = categoryBlogDAO.getAllCategoryBlog();
        if(result == null || result.isEmpty()){
            return null;
        }
        return result;
    }
    public CategoryBlog getCategoryBlogById(int id){
        return categoryBlogDAO.getCategoryBlogById(id);
    }
}
