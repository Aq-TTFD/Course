package services;

import entities.IActor;
import io.jsonwebtoken.*;

import java.util.Date;

public class TokenService {

    private final String JWT_SECRET = "U2FsdGVkX1/1nMZ54TUV7X9sdcY79Pi29MptWQ2xwQ0=";

    private final long JWT_EXPIRATION = 900000L; // 15 minutes

    public String generateToken(IActor actor) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION);

        // Create a Claims object and set the necessary claims
        Claims claims = Jwts.claims();
        claims.setSubject(actor.getId().toString());
        claims.put("email", actor.getEmail());
        if(actor.getPassword() != null) {
            claims.put("oldPassword", actor.getPassword().substring(10, 30));
        }
        claims.setIssuedAt(now);
        claims.setExpiration(expiryDate);

        // Build and return the JWT
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }


    public Integer getActorId(String authToken) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(authToken)
                .getBody();
        return Integer.parseInt(claims.getSubject());
    }

    public String getActorEmail(String authToken) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(authToken)
                .getBody();
        return (String) claims.get("email");
    }

    public String getOldPassword(String authToken) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(authToken)
                .getBody();
        return (String) claims.get("oldPassword");
    }


    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            System.out.println("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            System.out.println("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            System.out.println("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            System.out.println("JWT claims string is empty.");
        }
        return false;
    }
}
