package services;

import daos.StudentDAO;
import entities.Student;
import org.mindrot.jbcrypt.BCrypt;

import java.util.regex.Pattern;

public class StudentService {
    private static StudentService instance;

    private StudentDAO studentDAO;
    private StudentService(){
        studentDAO = new StudentDAO();
    }

    public static StudentService getInstance(){
        if(instance == null){
            instance = new StudentService();
        }
        return instance;
    }

    public Student getStudentByEmailAndPassword(String email, String password){
        Student student = studentDAO.findByEmail(email);
        if(student == null){
            return null;
        }
        if(student.getPassword() == null || student.getPassword().isEmpty()){
            return null;
        }
        if(!BCrypt.checkpw(password, student.getPassword())){
            return null;
        }
        return student;
    }

    public String registerStudentAccount(String name, String email, String password){
        Student student = new Student();
        student.setFullName(name);
        if(!isValidName(name)){
            return "Invalid name";
        }
        if(!isValidEmail(email)){
            return "Invalid email";
        }
        if(!isValidPassword(password)){
            return "Invalid password format";
        }
        student.setEmail(email);
        student.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        if( studentDAO.saveNewRegisterStudent(student) == null){
            return "Failed to register";
        }
        return "Success!";
    }

    private boolean isValidName(String name){
        if (name == null || name.isEmpty()) {
            return false;
        }

        String emailRegex = "([a-zA-Z0-9_\\s]+)";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(name).matches();
    }

    private boolean isValidEmail(String email){
        if (email == null || email.isEmpty()) {
            return false;
        }

        String emailRegex = "[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(email).matches();
    }

    private boolean isValidPassword(String password){
        if(password == null || password.isEmpty()){
            return false;
        }
        String emailRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(password).matches();
    }

    public void insertNewStudent(String email,String password, String fullname, String datefbirth,String enrollmentdate){
        Student student = new Student();
        student.setEmail(email);
        student.setPassword(password);
        student.setFullName(fullname);
        student.setDateOfBirth(datefbirth);
        student.setEnrollmentDate(enrollmentdate);
        student.setRoleId(3);
        studentDAO.addStudent(student);
    }

    public Student getStudentByEmail(String email){
        return studentDAO.findByEmail(email);
    }

    public boolean syncStudentAccount(Student student){
        Student regisStudent = studentDAO.findByEmail(student.getEmail());
        int result = 0;
        if(regisStudent == null){
            result = studentDAO.saveStudent(student);
        } else {
            result = studentDAO.updateStudentByEmail(student);
        }
        if(result == 0){
            System.out.println("Failed to sync student account");
        }
        return result != 0;
    }

    public boolean updatePasswordStudent(Student student){
        student.setPassword(BCrypt.hashpw(student.getPassword(), BCrypt.gensalt()));
        int result = studentDAO.updatePasswordByEmail(student);
        if(result == 0){
            System.out.println("Failed to update student account");
        }
        return result != 0;
    }

}

