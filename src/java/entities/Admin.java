package entities;

public class Admin implements IActor {
    private Integer id;
    private String name;
    private String email;
    private Boolean active;
    private String password;
    private String address;
    private Boolean gender;
    private String img_URL;
    private String phone;
    private Integer roleId;

    public Admin() {

    }

    public Admin(Integer id, String name, String email, Boolean active, String password, String address, Boolean gender, String img_URL, String phone, Integer roleId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.active = active;
        this.password = password;
        this.address = address;
        this.gender = gender;
        this.img_URL = img_URL;
        this.phone = phone;
        this.roleId = roleId;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getImg_URL() {
        return img_URL;
    }

    public void setImg_URL(String img_URL) {
        this.img_URL = img_URL;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}