/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

/**
 *
 * @author quang huy
 */
public class Lecture {
    private int id;
    private String name;
    private int time;
    private String description;
    private String imageURL;
    private String videoURL;
    private int courseID;

    public Lecture(int id, String name, int time, String description, String imageURL, String videoURL, int courseID) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.description = description;
        this.imageURL = imageURL;
        this.videoURL = videoURL;
        this.courseID = courseID;
    }

    public Lecture(int id, String name, int time, String description, String imageURL, String videoURL) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.description = description;
        this.imageURL = imageURL;
        this.videoURL = videoURL;
    }
    
    

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    @Override
    public String toString() {
        return "Lecture{" + "id=" + id + ", name=" + name + ", time=" + time + ", description=" + description + ", imageURL=" + imageURL + ", videoURL=" + videoURL + ", courseID=" + courseID + '}';
    }

    

}
