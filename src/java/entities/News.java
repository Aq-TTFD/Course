/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

/**
 *
 * @author FPT SHOP
 */
public class News {
    private int id;
    private String tilte;
    private String createdDate;
    private String lecturer;
    private String shortDescription;
    private int totalOfComment;
    private String imgURL;

    public News() {
    }

    public News(int id, String tilte, String createdDate, String lecturer, String shortDescription, int totalOfComment, String imgURL) {
        this.id = id;
        this.tilte = tilte;
        this.createdDate = createdDate;
        this.lecturer = lecturer;
        this.shortDescription = shortDescription;
        this.totalOfComment = totalOfComment;
        this.imgURL = imgURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTilte() {
        return tilte;
    }

    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getTotalOfComment() {
        return totalOfComment;
    }

    public void setTotalOfComment(int totalOfComment) {
        this.totalOfComment = totalOfComment;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }
    
}
