package entities;

public class Instructor implements IActor {

    private Integer id;
    private Boolean active;
    private String password;
    private String fullName;
    private String specialization;
    private String email;
    private Integer roleId;

    public Instructor() {
    }

    public Instructor(Integer id, Boolean active, String password, String fullName, String specialization, String email, Integer roleId) {
        this.id = id;
        this.active = active;
        this.password = password;
        this.fullName = fullName;
        this.specialization = specialization;
        this.email = email;
        this.roleId = roleId;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return this.fullName;
    }

    public void setName(String name) {
        this.fullName = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public int getRole() {
        return roleId;
    }

    public void setRole(int role) {
        this.roleId = role;
    }

    @Override
    public String toString() {
        return "Instructor{" + "id=" + id + ", active=" + active + ", password=" + password + ", fullName=" + fullName + ", specialization=" + specialization + ", email=" + email + ", roleId=" + roleId + '}';
    }
    
    
}