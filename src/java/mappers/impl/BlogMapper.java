/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mappers.impl;

import entities.Blog;
import entities.CategoryBlog;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import mappers.RowMapperInterface;

/**
 *
 * @author khanh
 */
public class BlogMapper implements RowMapperInterface<Blog> {
         @Override
    public Blog mapRow(ResultSet rs) {
        try {
            Integer id = rs.getInt("blog_id");
            Integer idc = rs.getInt("category_id");
            Integer ida = rs.getInt("admin_id");
            String name = rs.getString("blog_details");
            String c = rs.getString("content");
            String i = rs.getString("img_URL");
            Date d = rs.getDate("publish_date");
            String t = rs.getString("title");
            
            return new Blog(id, c, i, name, t, d, ida, idc);
        } catch (SQLException e) {
            System.out.println("Error in CourseMapper: " + e.getMessage());
            return null;
        }
    }
    
}
