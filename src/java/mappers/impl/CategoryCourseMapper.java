/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mappers.impl;

import entities.CategoryCourse;
import java.sql.ResultSet;
import java.sql.SQLException;
import mappers.RowMapperInterface;

/**
 *
 * @author FPT SHOP
 */
public class CategoryCourseMapper implements RowMapperInterface<CategoryCourse>{
    
    @Override
    public CategoryCourse mapRow(ResultSet rs) {
        try {
            Integer id = rs.getInt("category_id");
            String name = rs.getString("category_name");        
            return new CategoryCourse(id, name);
        } catch (SQLException e) {
            System.out.println("Error in CourseMapper: " + e.getMessage());
            return null;
        }
    }
}
