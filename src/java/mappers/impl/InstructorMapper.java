package mappers.impl;

import entities.Instructor;
import mappers.RowMapperInterface;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InstructorMapper implements RowMapperInterface<Instructor> {
    @Override
    public Instructor mapRow(ResultSet rs) {
        try {
            Instructor instructor = new Instructor();
            instructor.setId(rs.getInt("instructor_id"));
            instructor.setActive(rs.getBoolean("active"));
            instructor.setPassword(rs.getString("password"));
            instructor.setFullName(rs.getString("full_name"));
            instructor.setSpecialization(rs.getString("specialization"));
            instructor.setEmail(rs.getString("email"));
            instructor.setRoleId(rs.getInt("role_id"));
            return instructor;
        } catch (SQLException e) {
            System.out.println("Error in ManagerMapper: " + e.getMessage());
            return null;
        }
    }
}
