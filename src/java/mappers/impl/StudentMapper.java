package mappers.impl;

import entities.Student;
import mappers.RowMapperInterface;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentMapper implements RowMapperInterface<Student> {
    @Override
    public Student mapRow(ResultSet rs) {
        try {
            Student student = new Student();
            student.setId(rs.getInt("student_id"));
            student.setEmail(rs.getString("email"));
            student.setEmailVerified(rs.getBoolean("email_verified"));
            student.setPhoneNumber(rs.getString("phone_number"));
            student.setActive(rs.getBoolean("active"));
            student.setPassword(rs.getString("password"));
            student.setFullName(rs.getString("full_name"));
            student.setDateOfBirth(rs.getString("date_of_birth"));
            student.setPictureUrl(rs.getString("picture_url"));
            student.setFamilyName(rs.getString("family_name"));
            student.setGivenName(rs.getString("given_name"));
            student.setEnrollmentDate(rs.getString("enrollment_date"));
            student.setRoleId(rs.getInt("role_id"));
            return student;
        } catch (SQLException e) {
            System.out.println("Error in ManagerMapper: " + e.getMessage());
            return null;
        }
    }
}
