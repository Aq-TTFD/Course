/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mappers.impl;

import entities.CategoryBlog;
import entities.CategoryCourse;
import java.sql.ResultSet;
import java.sql.SQLException;
import mappers.RowMapperInterface;

/**
 *
 * @author khanh
 */
public class CategoryBlogMapper  implements RowMapperInterface<CategoryBlog> {
       @Override
    public CategoryBlog mapRow(ResultSet rs) {
        try {
            Integer id = rs.getInt("category_id");
            String name = rs.getString("category_name");        
            return new CategoryBlog(id, name);
        } catch (SQLException e) {
            System.out.println("Error in CourseMapper: " + e.getMessage());
            return null;
        }
    }
    
}
