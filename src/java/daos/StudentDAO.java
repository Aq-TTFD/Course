package daos;

import entities.Student;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import mappers.impl.StudentMapper;

import java.util.List;

public class StudentDAO extends GenericDAO<Student> {

    private final String FIND_STUDENT_BY_EMAIL = "SELECT * FROM student WHERE email = ? AND active = 1";
    private final String SAVE_NEW_REGISTER_STUDENT = "INSERT INTO [dbo].[student] (email, password, full_name, role_id) VALUES (?, ?, ?, 3)";
    private final String SAVE_STUDENT_STATEMENT = "INSERT INTO student (email, email_verified, full_name, picture_Url, family_name, given_name, role_id) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";

    public Integer saveNewRegisterStudent(Student student) {
        return executeUpdate(SAVE_NEW_REGISTER_STUDENT, student.getEmail(), student.getPassword(), student.getFullName());
    }

    public Integer saveStudent(Student student) {

        return executeUpdate(SAVE_STUDENT_STATEMENT, student.getEmail(), student.getEmailVerified(),
                student.getFullName(), student.getPictureUrl(), student.getFamilyName(),
                student.getGivenName(), 3);
    }
    
    public Integer updatePasswordByEmail(Student student){
        String sql = "UPDATE student SET password = ? WHERE email = ?";
        return executeUpdate(sql, student.getPassword(), student.getEmail());
    }

    public Integer updateStudentByEmail(Student student) {
        String sql = "UPDATE student SET email_verified = ?, picture_Url = ?, family_name = ?, given_name = ? WHERE email = ?";

        return executeUpdate(sql, student.getEmailVerified(), student.getPictureUrl(),
                student.getFamilyName(), student.getGivenName(), student.getEmail());
    }

    public Student findByEmail(String email) {
        List<Student> result = executeQuery(FIND_STUDENT_BY_EMAIL, new StudentMapper(), email);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public List<Student> getAll() {
        String sql = "select * from student";
        List<Student> list = executeQuery(sql, new StudentMapper());
        return list;
    }

    public void deleteStudent(int sid) {
        String sql = "DELETE FROM student WHERE student_id = ?";
        Integer id = executeUpdate(sql, sid);
        if (id != null) {
            System.out.println("Student with ID " + sid + " has been deleted successfully.");
        } else {
            System.out.println("Failed to delete student with ID " + sid + ".");
        }

    }

    public Student addStudent(Student student) {
        String sql = "INSERT INTO [dbo].[student]\n"
                + "           ([email]\n"
                + "           ,[email_verified]\n"
                + "           ,[phone_number]\n"
                + "           ,[active]\n"
                + "           ,[password]\n"
                + "           ,[full_name]\n"
                + "           ,[date_of_birth]\n"
                + "           ,[picture_Url]\n"
                + "           ,[family_name]\n"
                + "           ,[given_name]\n"
                + "           ,[enrollment_date]\n"
                + "           ,[role_id])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?,?,?,?)";
        Integer generatedId = executeUpdate(sql,
                student.getEmail(),
                student.getEmailVerified(),
                student.getPhoneNumber(),
                student.getActive(),
                student.getPassword(),
                student.getFullName(),
                student.getDateOfBirth(),
                student.getPictureUrl(),
                student.getFamilyName(),
                student.getGivenName(),
                student.getEnrollmentDate(),
                student.getRoleId()
        );
        if (generatedId != null) {
            student.setId(generatedId);
        }

        return student;
    }

    public Student updateStudent(Student student) {
        String sql = "UPDATE [dbo].[student]\n"
                + "   SET [email] = ?\n"
                + "      ,[email_verified] = ?\n"
                + "      ,[phone_number] = ?\n"
                + "      ,[active] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[full_name] = ?\n"
                + "      ,[date_of_birth] = ?\n"
                + "      ,[picture_Url] = ?\n"
                + "      ,[family_name] = ?\n"
                + "      ,[given_name] = ?\n"
                + "      ,[enrollment_date] = ?\n"
                + "      ,[role_id] = ?\n"
                + " WHERE student_id = ?";
        Integer generatedId = executeUpdate(sql,
                student.getEmail(),
                student.getEmailVerified(),
                student.getPhoneNumber(),
                student.getActive(),
                student.getPassword(),
                student.getFullName(),
                student.getDateOfBirth(),
                student.getPictureUrl(),
                student.getFamilyName(),
                student.getGivenName(),
                student.getEnrollmentDate(),
                student.getRoleId(),
                student.getId()
        );
        if (generatedId != null) {
            student.setId(generatedId);
        }

        return student;
    }

    public Student getStudentById(int student_id) {
        String sql = "select * from student where student_id =?";
        List<Student> result = executeQuery(sql, new StudentMapper(), student_id);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<Student> getListPage(List<Student> list, int start, int end) {
        ArrayList<Student> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;

    }


    public List<Student> searchByName(String key) {
        String sql = "SELECT * FROM student WHERE full_name LIKE ?";
        return executeQuery(sql, new StudentMapper(), "%" + key + "%");
    }

    public static void main(String[] args) {
        StudentDAO dao = new StudentDAO();
        List<Student> list = dao.searchByName("linh");
        for (Student student : list) {
            System.out.println(student);
        }
//        dao.updateStudent(new Student(7, "homnayancut@gmail.com", Boolean.FALSE, "0987216161", Boolean.TRUE, "khongco", "toi te", "2000-12-12", "hienchuaco", "day roi", "day roi", "2021-12-12", 1));
        System.out.println("xong");
    }
}
