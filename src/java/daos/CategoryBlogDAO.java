package daos;

import entities.CategoryBlog;
import entities.CategoryCourse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import mappers.impl.CategoryBlogMapper;
import mappers.impl.CategoryCourseMapper;
import utils.DBContext;

public class CategoryBlogDAO extends GenericDAO<CategoryBlog> {

    private final String SELECT_ALL_CATEGORY_BLOG_STATEMENT = "SELECT * FROM category_blog";
    private final String SELECT_CATEGORY_BY_ID_STATEMENT = "SELECT * FROM category_blog where category_id = ?";

    public List<CategoryBlog> getAllCategoryBlog() {
        List<CategoryBlog> result = executeQuery(SELECT_ALL_CATEGORY_BLOG_STATEMENT, new CategoryBlogMapper());
        return result.isEmpty() ? null : result;
    }

    public CategoryBlog getCategoryBlogById(int id) {
        List<CategoryBlog> result = executeQuery(SELECT_CATEGORY_BY_ID_STATEMENT, new CategoryBlogMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }

//    public static void main(String[] args) {
//        CategoryBlogDAO c = new CategoryBlogDAO();
//        List<CategoryBlog> list = c.getAllCategoryBlog();
//        System.out.println(list.get(0).getCategory_name());
//    }

}
