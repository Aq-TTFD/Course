/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.CategoryCourse;
import java.util.ArrayList;
import java.util.List;
import mappers.impl.CategoryCourseMapper;

/**
 *
 * @author FPT SHOP
 */
public class CategoryCourseDAO extends GenericDAO<CategoryCourse> {

    private final String SELECT_ALL_CATEGORY_COURSE_STATEMENT = "SELECT * FROM category_course";
    private final String SELECT_CATEGORY_BY_ID_STATEMENT = "SELECT * FROM category_course where category_id = ?";
    private final String SELECT_CATEGORY_BY_COURSEID_STATEMENT = "SELECT *\n"
            + "FROM [dbo].[course] c\n"
            + "INNER JOIN [dbo].[category_course] cc ON c.[category_id] = cc.[category_id]\n"
            + "WHERE c.[course_id] = ?";

    public List<CategoryCourse> getAllCategoryCourse() {
        List<CategoryCourse> result = executeQuery(SELECT_ALL_CATEGORY_COURSE_STATEMENT, new CategoryCourseMapper());
        return result.isEmpty() ? null : result;
    }

    public CategoryCourse getCategoryCourseById(int id) {
        List<CategoryCourse> result = executeQuery(SELECT_CATEGORY_BY_ID_STATEMENT, new CategoryCourseMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }
    public CategoryCourse getCategoryCourseByCourseId(int courseId) {
        List<CategoryCourse> result = executeQuery(SELECT_CATEGORY_BY_COURSEID_STATEMENT, new CategoryCourseMapper(), courseId);
        return result.isEmpty() ? null : result.get(0);
    }

}
