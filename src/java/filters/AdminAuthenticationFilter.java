package filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.JwtTokenProvider;

import java.io.IOException;

@WebFilter(filterName = "AdminAuthenticationFilter",
        urlPatterns = {"/admin-dashboard","/AddCategoryBlogController"},
        dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class AdminAuthenticationFilter implements Filter {
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        jwtTokenProvider = new JwtTokenProvider();
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
// Check cookie has session ID or not
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        Cookie authenticationCookie = getAuthenticationCookie(req);
        if(authenticationCookie != null) {
            if(isCookieValid(authenticationCookie)){
                if(isRoleAdmin(authenticationCookie)){
                    servletRequest.setAttribute("admin_id", getAdminId(authenticationCookie));
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            } else {
                Cookie newCookie = new Cookie("admin_token", "");
                newCookie.setMaxAge(0);
                newCookie.setDomain("localhost");
                newCookie.setPath("/FPT-Nihongo");
                newCookie.setHttpOnly(true);
                res.addCookie(newCookie);
            }
        }

        res.sendRedirect("login");
    }

    
    private int getAdminId(Cookie cookie){
        return jwtTokenProvider.getUserIdFromJWT(cookie.getValue());
    }
    
    private boolean isCookieValid(Cookie cookie){
        return jwtTokenProvider.validateToken(cookie.getValue());
    }

    private boolean isRoleAdmin(Cookie cookie){
        return jwtTokenProvider.getRoleFromJWT(cookie.getValue()).equals("1");
    }

    private Cookie getAuthenticationCookie(HttpServletRequest req){
        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("admin_token")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
